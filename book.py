from config import DBconnection

class Book:

    def __init__(self, author, title, price, pages, amount):
        self.idbooks = None
        self.author = author
        self.title = title
        self.price = price
        self.pages = pages
        self.amount = amount

    def __str__(self):
        return f"Id: {self.idbooks}, \n" f"Autor: {self.author}, \n" f"Tytuł: {self.title}, \n" f"Cena : {self.price}, \n" f"Ilość stron: {self.pages}"

    def save(self):
        db = DBconnection.connect()

        sql = "INSERT INTO books (author, title, price, pages, amount) VALUES(%s, %s, %s, %s, %s)"
        values = (self.author, self.title, self.price, self.pages, self.amount)

        cursor = db.cursor()
        cursor.execute(sql, values)
        db.commit()

        print("Zapisano książkę. Id książki to: ",cursor.lastrowid)
        self.idbooks = cursor.lastrowid
    
    @staticmethod #nie ma odniesienia do samego siebie
    def get_one(idbooks):
        db = DBconnection.connect()

        sql = "SELECT * FROM books WHERE idbooks = '%s'"
        value = [idbooks] # lub krotka z przecinkiem...

        cursor = db.cursor()
        cursor.execute(sql, value)
        book_record = cursor.fetchone()

        return Book.__converter_row_to_object(book_record)
  
    @staticmethod
    def __converter_row_to_object(book_record):
        idbooks = book_record[0]
        author = book_record[1]
        title = book_record[2]
        price = book_record[3]
        pages = book_record[4]
        amount = book_record[5]
        #book = Book(book_record[1], book_record[2], book_record[3], book_record[4], book_record[5])
        book = Book(author, title, price, pages, amount)
        book.idbooks = idbooks
        return book

    @staticmethod 
    def get_all():
        db = DBconnection.connect()

        sql = "SELECT * FROM books"

        cursor = db.cursor()
        cursor.execute(sql)
        book_records = cursor.fetchall()
        
        book_list = []
        for record in book_records:
            book_list.append(Book.__converter_row_to_object(record))

        return book_list

    @staticmethod 
    def update(self):
        if self.idbooks == None:
            raise Exception("Id nie może być None")
        db = DBconnection.connect()

        sql = "UPDATE books SET author = %s, title = %s, price = %s, pages = %s, amount = %s WHERE idbooks = %s"
        value = (self.author, self.title, self.price, self.pages, self.amount, self.idbooks)

        cursor = db.cursor()
        cursor.execute(sql, value)
        db.commit()

        print("Książka została zaktualizowana")