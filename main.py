from book import Book

def show_menu():
    print("""
    1. Utwórz książkę
    2. Lista książek
    3. Zaktualizuj książkę
    """)

def get_choice():
    while True:
        try:
            choice = int(input())
            if choice > 0 and choice < 4:
                return choice
        except:
            pass
    
        print("Podano nieprawidłową wartość")
        show_menu()

def create_book():
    author = input("Podaj autora: ")
    title = input("Podaj tytuł: ")
    price = input("Podaj cenę: ")
    pages = input("Podaj liczbę stron: ")
    amount = input("Podaj liczbę książek: ")
    book = Book(author, title, price, pages, amount)
    book.save()

def books_list():
    books = Book.get_all()
    
    for book in books:
        print(book)

def update_book():
    id = int(input("Podaj ID książki, którą chcesz zaktualizować: "))
    book = None #trzeba zadeklarować przed try

    try:
        book = Book.get_one(id)
        print(book)
    except:
        print("Książka o podanym ID nie istnieje")
        return

    update_author = input("Czy chcesz zaktualizować autora? t/n ")
    if update_author[0].lower() == "t":
        author = input("Podaj autora: ")
        book.author = author
    update_title = input("Czy chcesz zaktualizować tytuł? t/n ")
    if update_title[0].lower() == "t":
        title = input("Podaj tytuł: ")
        book.title = title
    Book.update(book)

while True:
    show_menu()
    choice = get_choice()
    if choice == 1:
        create_book()
    elif choice == 2:
        books_list()
    elif choice == 3:
        update_book()
