from book import Book

books = []

def show_menu():

    while True:
        print("""
        1. Utwórz książkę
        2. Wybierz książkę
        3. Lista książek
        4. Wyszukaj książkę
        """)

        choice = int(input())

        if choice == 1:
            createBook()
        elif choice == 3:
            bookList()
        elif choice == 4:
            searchBook()

def createBook():
    global books
    author = input("Podaj autora: ")
    title = input("Podaj tytuł książki: ")
    price = input("Podaj cenę: ")
    pages = input("Podaj ilość stron: ")

    book = Book(author, title, price, pages)
    books.append(book)

def bookList():
    global books

    print("====LISTA KSIĄŻEK====")

    for book in books:
        print((str(book) + "\n---------------------\n"))

def searchBook():
    global books

    author = input("Podaj autora: ")
    title = input("Podaj tytuł: ")

    for book in books:
        if book.author == author and book.title == title:
            print(str(book) + "\n")

