import mysql.connector

class DBconnection:
    
    #oznaczenie metody jako statycznej, czyli metoda może być wywyłoana bez koneiczności tworzenia nowego obiektu
    @staticmethod
    def connect():
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="root",
            database="ksiegarnia",
            port="3306"
        )

        return mydb